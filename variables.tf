variable "applications" {
  description = "List of applications that need a ecr registry"
  type        = list
}

variable "expiration_window" {
  description = "Expire images older than"
  default     = 10
}

variable "product_name" {
    description = "Product name"
}
