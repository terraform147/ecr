# Terraform ECR Module

## Contains

- Registry images

## Outputs

- ECR Url's

## How to use

### Setup Module
```
module "ecr" {
  source = "git@gitlab.com:terraform147/ecr.git"
  applications = []
}
```

### Import module

```
terraform init
```

## Give a star :stars:

## Want to improve or fix something? Fork me and send a MR! :punch: